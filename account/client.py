from typing import Optional

import aiohttp
from ujson import loads as json_loads

from .errors import API_ERROR_REGISTRY
from .methods import (
    GroupMethods,
    PermissionMethods,
    TokenMethods,
    UserMethods
)


class AccountClient:
    def __init__(self,
                 base_url: str,
                 api_version: str = 'v1',
                 encoding: str = 'utf-8'):
        self.__base_url = base_url.rstrip('/')
        self.__api_version = api_version
        self.__base_endpoint = f'{self.__base_url}/api/{self.__api_version}'
        self.__encoding = encoding

        self.groups: GroupMethods = GroupMethods(client=self)
        self.permissions: PermissionMethods = PermissionMethods(client=self)
        self.tokens: TokenMethods = TokenMethods(client=self)
        self.users: UserMethods = UserMethods(client=self)

    async def request(self,
                      method: str,
                      http_method: str,
                      params: Optional[dict] = None,
                      data: Optional[dict] = None,
                      json: Optional[dict] = None) -> Optional[dict]:
        url = f'{self.__base_endpoint}/{method}'
        async with aiohttp.ClientSession() as session:
            async with session.request(method=http_method,
                                       url=url,
                                       params=params,
                                       data=data,
                                       json=json) as request:
                response = json_loads(await request.text(encoding=self.__encoding))
                if not response['success']:
                    error_code = response['error']['code']
                    error_message = response['error']['message']
                    raise API_ERROR_REGISTRY[error_code](error_message=error_message)
                return response.get('data')


__all__ = ['AccountClient']
