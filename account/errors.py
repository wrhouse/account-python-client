class APIError(Exception):
    error_code: int = None
    error_message: str = None

    def __init__(self, error_message: str):
        super().__init__(f'API Error #{self.error_code}: {error_message}')
        self.error_message = error_message


class InternalServerError(APIError):
    error_code = 1000


class MissingParameter(APIError):
    error_code = 1001


class InvalidParameter(APIError):
    error_code = 1002


class DuplicateError(APIError):
    error_code = 1003


class ValidationParametersError(APIError):
    error_code = 1004


class AccessTokenExpired(APIError):
    error_code = 1005


class AccessTokenDeleted(APIError):
    error_code = 1006


class RefreshTokenExpired(APIError):
    error_code = 1007


API_ERROR_REGISTRY = {
    InternalServerError.error_code: InternalServerError,
    MissingParameter.error_code: MissingParameter,
    InvalidParameter.error_code: InvalidParameter,
    DuplicateError.error_code: DuplicateError,
    ValidationParametersError.error_code: ValidationParametersError,
    AccessTokenExpired.error_code: AccessTokenExpired,
    AccessTokenDeleted.error_code: AccessTokenDeleted,
    RefreshTokenExpired.error_code: RefreshTokenExpired
}