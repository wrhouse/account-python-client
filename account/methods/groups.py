from typing import Optional, List

from ..models import Group
from ..utils import clear_dict, replace_empty_with_none


class GroupMethods:
    def __init__(self, client):
        self.__client = client
        self.__base_method = 'group'

    async def get_groups(self,
                         *,
                         fields: Optional[List[str]] = None) -> List[Group]:
        params = dict()
        if fields is not None:
            params['fields'] = ','.join(fields)
        data = await self.__client.request(method=self.__base_method,
                                           http_method='GET',
                                           params=params)
        return [Group(**group) for group in data['groups']]

    async def create_or_update(self,
                               *,
                               group_id: str,
                               name: str,
                               description: Optional[str] = None):
        json = {
            'name': name,
            'description': description
        }
        json = replace_empty_with_none(json,
                                       'name')

        clear_dict(json)
        await self.__client.request(method=f'{self.__base_method}/{group_id}',
                                    http_method='POST',
                                    json=json)

    async def delete(self,
                     *,
                     group_id: str):
        await self.__client.request(method=f'{self.__base_method}/{group_id}',
                                    http_method='DELETE')

    async def add_member(self,
                         *,
                         group_id: str,
                         user_id: int):
        await self.__client.request(method=f'{self.__base_method}/{group_id}/users/{user_id}',
                                    http_method='POST')

    async def remove_member(self,
                            *,
                            group_id: str,
                            user_id: int):
        await self.__client.request(method=f'{self.__base_method}/{group_id}/users/{user_id}',
                                    http_method='DELETE')

    async def get_permission_ids(self,
                                 *,
                                 group_id: str) -> List[str]:
        data = await self.__client.request(method=f'{self.__base_method}/{group_id}/permissions',
                                           http_method='GET')
        return data['permission_ids']

    async def get_member_ids(self,
                             *,
                             group_id: str) -> List[int]:
        data = await self.__client.request(method=f'{self.__base_method}/{group_id}/users',
                                           http_method='GET')
        return data['user_ids']


__all__ = ['GroupMethods']
