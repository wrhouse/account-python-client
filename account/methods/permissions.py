from typing import Optional, List

from ..models import Permission
from ..utils import clear_dict


class PermissionMethods:
    def __init__(self, client):
        self.__client = client
        self.__base_method = 'permission'

    async def get_permissions(self,
                              *,
                              fields: Optional[List[str]] = None) -> List[Permission]:
        params = dict()
        if fields is not None:
            params['fields'] = ','.join(fields)
        data = await self.__client.request(method=self.__base_method,
                                           http_method='GET',
                                           params=params)
        return [Permission(**permission) for permission in data['permissions']]

    async def create_or_update(self,
                               *,
                               permission_id: str,
                               description: Optional[str] = None):
        json = {
            'description': description
        }
        clear_dict(json)
        await self.__client.request(method=f'{self.__base_method}/{permission_id}',
                                    http_method='POST',
                                    json=json)

    async def delete(self,
                     *,
                     permission_id: str):
        await self.__client.request(method=f'{self.__base_method}/{permission_id}',
                                    http_method='DELETE')

    async def add_group(self,
                        *,
                        permission_id: str,
                        group_id: str):
        await self.__client.request(method=f'{self.__base_method}/{permission_id}/groups/{group_id}',
                                    http_method='POST')

    async def remove_group(self,
                           *,
                           permission_id: str,
                           group_id: str):
        await self.__client.request(method=f'{self.__base_method}/{permission_id}/groups/{group_id}',
                                    http_method='DELETE')

    async def add_user(self,
                       *,
                       permission_id: str,
                       user_id: int):
        await self.__client.request(method=f'{self.__base_method}/{permission_id}/users/{user_id}',
                                    http_method='POST')

    async def remove_user(self,
                          *,
                          permission_id: str,
                          user_id: int):
        await self.__client.request(method=f'{self.__base_method}/{permission_id}/users/{user_id}',
                                    http_method='DELETE')


__all__ = ['PermissionMethods']
