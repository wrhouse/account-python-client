from typing import Optional, List

from ..models import TokenPair, Token
from ..utils import clear_dict, replace_empty_with_none


class TokenMethods:
    def __init__(self, client):
        self.__client = client
        self.__base_method = 'token'

    async def get_user_tokens(self,
                              *,
                              user_id: int,
                              is_deleted: Optional[bool] = None,
                              is_expired: Optional[bool] = None,
                              fields: Optional[List[str]] = None) -> List[Token]:
        params = {
            'is_deleted': is_deleted,
            'is_expired': is_expired
        }
        if fields is not None:
            params['fields'] = ','.join(fields)
        clear_dict(params)
        data = await self.__client.request(method=f'{self.__base_method}/users/{user_id}',
                                           http_method='GET',
                                           params=params)
        return [Token(**token) for token in data['tokens']]

    async def create_token_pair(self,
                                *,
                                user_id: int,
                                ip_address: str,
                                user_agent: str) -> TokenPair:
        json = {
            'ip_address': ip_address,
            'user_agent': user_agent
        }
        json = replace_empty_with_none(json,
                                       'ip_address',
                                       'user_agent')

        data = await self.__client.request(method=f'{self.__base_method}/users/{user_id}',
                                           http_method='POST',
                                           json=json)
        return TokenPair(**data)

    async def refresh(self,
                      *,
                      refresh_token: str) -> TokenPair:
        json = {
            'refresh_token': refresh_token
        }
        json = replace_empty_with_none(json,
                                       'refresh_token')
        data = await self.__client.request(method=self.__base_method,
                                           http_method='PUT',
                                           json=json)
        return TokenPair(**data)

    async def validate(self,
                       *,
                       access_token: str) -> bool:
        params = {
            'access_token': access_token
        }
        params = replace_empty_with_none(params,
                                         'access_token')

        await self.__client.request(method=self.__base_method,
                                    http_method='GET',
                                    params=params)
        return True

    async def delete_token(self,
                           *,
                           token_id: Optional[str] = None,
                           access_token: Optional[str] = None):
        json = {
            'token_id': token_id,
            'access_token': access_token
        }
        json = replace_empty_with_none(json,
                                       'token_id',
                                       'access_token')

        clear_dict(json)
        await self.__client.request(method=self.__base_method,
                                    http_method='DELETE',
                                    json=json)

    async def delete_all_user_tokens(self,
                                     *,
                                     user_id: int,
                                     whitelist: Optional[List[str]] = None):
        json = {
            'whitelist': whitelist
        }
        json = replace_empty_with_none(json,
                                       'whitelist')
        clear_dict(json)
        await self.__client.request(method=f'{self.__base_method}/users/{user_id}',
                                    http_method='DELETE',
                                    json=json)


__all__ = ['TokenMethods']
