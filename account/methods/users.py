from datetime import date, datetime
from typing import Optional, List, Dict, Union

from ..models import User, GenderTypes, UserFields
from ..utils import clear_dict, replace_empty_with_none


class UserMethods:
    def __init__(self, client):
        self.__client = client
        self.__base_method = 'user'

    async def get_user_info(self,
                            *,
                            user_id: int,
                            service_id: str = 'profile',
                            fields: Optional[List[UserFields]] = None) -> User:
        params = dict(service_id=service_id)
        if fields is not None:
            params['fields'] = ','.join([field.value for field in fields])
        data = await self.__client.request(method=f'{self.__base_method}/{user_id}',
                                           http_method='GET',
                                           params=params)
        return User(**data)

    async def get_users(self,
                        *,
                        count: int = 20,
                        offset: int = 0,
                        rev: bool = False,
                        fields: Optional[List[UserFields]] = None) -> List[User]:
        params = {
            'count': count,
            'offset': offset,
            'rev': rev
        }
        if fields is not None:
            params['fields'] = ','.join([field.value for field in fields])
        data = await self.__client.request(method=self.__base_method,
                                           http_method='GET',
                                           params=params)
        return [User(**user) for user in data['users']]

    async def create(self,
                     *,
                     nickname: str,
                     email: str,
                     password: str,
                     name: Optional[str] = None,
                     surname: Optional[str] = None,
                     vk_id: Optional[str] = None,
                     twitter_id: Optional[str] = None,
                     facebook_id: Optional[str] = None,
                     google_id: Optional[str] = None) -> int:
        json = {
            'nickname': nickname,
            'email': email,
            'password': password,
            'name': name,
            'surname': surname,
            'vk_id': vk_id,
            'twitter_id': twitter_id,
            'facebook_id': facebook_id,
            'google_id': google_id
        }
        json = replace_empty_with_none(json,
                                       'name',
                                       'email',
                                       'surname',
                                       'password',
                                       'nickname')

        clear_dict(json)
        data = await self.__client.request(method=self.__base_method,
                                           http_method='POST',
                                           json=json)
        return data['user_id']

    async def search(self,
                     *,
                     nickname: Optional[str] = None,
                     email: Optional[str] = None,
                     phone: Optional[str] = None,
                     vk_id: Optional[str] = None,
                     twitter_id: Optional[str] = None,
                     facebook_id: Optional[str] = None,
                     google_id: Optional[str] = None,
                     fields: Optional[List[UserFields]] = None) -> List[User]:
        payload = {
            'nickname': nickname,
            'email': email,
            'phone': phone,
            'vk_id': vk_id,
            'twitter_id': twitter_id,
            'facebook_id': facebook_id,
            'google_id': google_id,
        }
        clear_dict(payload)
        if fields:
            payload['fields'] = ",".join([field.value for field in fields])
        data = await self.__client.request(method=f'{self.__base_method}/search',
                                           http_method='GET',
                                           params=payload)
        return [User(**user) for user in data['users']]

    async def update(self,
                     *,
                     user_id: int,
                     shop_user_id: Optional[int] = None,
                     forum_user_id: Optional[int] = None,
                     nickname: Optional[str] = None,
                     name: Optional[str] = None,
                     surname: Optional[str] = None,
                     email: Optional[str] = None,
                     phone: Optional[str] = None,
                     password: Optional[str] = None,
                     vk_id: Optional[str] = None,
                     twitter_id: Optional[str] = None,
                     facebook_id: Optional[str] = None,
                     google_id: Optional[str] = None,
                     gender: Optional[Union[GenderTypes, str]] = None,
                     birth_date: Optional[Union[date, str]] = None,
                     social: Optional[Dict[str, str]] = None,
                     image: Optional[str] = None):
        json = {
            'shop_user_id': shop_user_id,
            'forum_user_id': forum_user_id,
            'nickname': nickname,
            'name': name,
            'surname': surname,
            'email': email,
            'phone': phone,
            'password': password,
            'vk_id': vk_id,
            'twitter_id': twitter_id,
            'facebook_id': facebook_id,
            'google_id': google_id,
            'social': social,
            'image': image
        }
        if gender is not None:
            if type(gender) == str:
                gender = GenderTypes[gender]
            json['gender'] = gender.value
        if birth_date is not None:
            if type(birth_date) == str:
                birth_date = datetime.fromisoformat(birth_date)
            json['birth_date'] = birth_date.isoformat()
        json = replace_empty_with_none(json,
                                       'name',
                                       'email',
                                       'surname',
                                       'password',
                                       'nickname')

        clear_dict(json)
        await self.__client.request(method=f'{self.__base_method}/{user_id}',
                                    http_method='POST',
                                    json=json)

    async def verify_email(self,
                           *,
                           user_id: int):
        await self.__client.request(method=f'{self.__base_method}/{user_id}/verify/email',
                                    http_method='POST')

    async def verify_phone(self,
                           *,
                           user_id: int):
        await self.__client.request(method=f'{self.__base_method}/{user_id}/verify/phone',
                                    http_method='POST')

    async def get_group_ids(self,
                            *,
                            user_id: int,
                            service_id: str = 'profile') -> List[str]:
        params = {
            'service_id': service_id
        }
        data = await self.__client.request(method=f'{self.__base_method}/{user_id}/groups',
                                           http_method='GET',
                                           params=params)
        return data['group_ids']

    async def get_permission_ids(self,
                                 *,
                                 user_id: int,
                                 service_id: str = 'profile',
                                 show_group_permissions: bool = False) -> List[str]:
        params = {
            'service_id': service_id,
            'show_group_permissions': show_group_permissions
        }
        data = await self.__client.request(method=f'{self.__base_method}/{user_id}/permissions',
                                           http_method='GET',
                                           params=params)
        return data['permission_ids']

__all__ = ['UserMethods']
