from datetime import datetime, date
from enum import Enum
from typing import Optional, Dict


class Group:
    __slots__ = ('group_id', 'name', 'description', 'created_at')

    def __init__(self,
                 *,
                 group_id: Optional[str] = None,
                 name: Optional[str] = None,
                 description: Optional[str] = None,
                 created_at: Optional[str] = None):
        """
        :param group_id: Идентификатор группы пользователей
        :param name: Название группы пользователей
        :param description: Описание группы пользователей
        :param created_at: Дата и время создания группы пользователей (UTC+0)
        """
        self.group_id: Optional[str] = group_id
        self.name: Optional[str] = name
        self.description: Optional[str] = description
        self.created_at: Optional[datetime] = None
        if created_at is not None:
            self.created_at = datetime.fromisoformat(created_at)


class Permission:
    __slots__ = ('permission_id', 'description')

    def __init__(self,
                 *,
                 permission_id: Optional[str] = None,
                 description: Optional[str] = None):
        """
        :param permission_id: Идентификатор прав доступа
        :param description: Описание прав доступа
        """
        self.permission_id: Optional[str] = permission_id
        self.description: Optional[str] = description


class TokenPair:
    __slots__ = ('access_token', 'refresh_token')

    def __init__(self,
                 *,
                 access_token: str,
                 refresh_token: str):
        """
        :param access_token: Токен доступа пользователя
        :param refresh_token: Токен, необходимый для обновления пары (access_token, refresh_token)
        """
        self.access_token: str = access_token
        self.refresh_token: str = refresh_token


class Token:
    __slots__ = ('token_id', 'access_token', 'user_id', 'ip_address', 'user_agent', 'created_at', 'updated_at',
                 'is_expired', 'is_deleted')

    def __init__(self,
                 *,
                 token_id: Optional[str] = None,
                 access_token: Optional[str] = None,
                 user_id: Optional[int] = None,
                 ip_address: Optional[str] = None,
                 user_agent: Optional[str] = None,
                 created_at: Optional[str] = None,
                 updated_at: Optional[str] = None,
                 is_expired: Optional[bool] = None,
                 is_deleted: Optional[bool] = None):
        """
        :param token_id: Уникальный идентификатор токена (сессии)
        :param access_token: Токен доступа пользователя, соответствующий сессии
        :param user_id: Идентификатор пользователя, которому принадлежит токен (сессия)
        :param ip_address: IP адрес, с которого был авторизован пользователь
        :param user_agent: User-Agent браузера, с которого был авторизован пользователь
        :param created_at: Дата и время создания токена (UTC+0)
        :param updated_at: Дата и время последнего обновления токена (UTC+0)
        :param is_expired: Флаг, означающий, что токен устарел (true)
        :param is_deleted: Флаг, означающий, что токен был удален вручную (true)
        """
        self.token_id: Optional[str] = token_id
        self.access_token: Optional[str] = access_token
        self.user_id: Optional[int] = user_id
        self.ip_address: Optional[str] = ip_address
        self.user_agent: Optional[str] = user_agent
        self.created_at: Optional[datetime] = None
        if created_at is not None:
            self.created_at = datetime.fromisoformat(created_at)
        self.updated_at: Optional[datetime] = None
        if updated_at is not None:
            self.updated_at = datetime.fromisoformat(updated_at)
        self.is_expired: Optional[bool] = is_expired
        self.is_deleted: Optional[bool] = is_deleted


class GenderTypes(Enum):
    male = "male"
    female = "female"
    undefined = "undefined"


class UserFields(Enum):
    user_id = "user_id"
    shop_user_id = "shop_user_id"
    forum_user_id = "forum_user_id"
    nickname = "nickname"
    name = "name"
    surname = "surname"
    email = "email"
    email_is_verified = "email_is_verified"
    phone = "phone"
    phone_is_verified = "phone_is_verified"
    password = "password"
    vk_id = "vk_id"
    twitter_id = "twitter_id"
    facebook_id = "facebook_id"
    google_id = "google_id"
    gender = "gender"
    birth_date = "birth_date"
    social = "social"
    image_large = "image_large"
    image_small = "image_small"
    created_at = "created_at"


class User:
    __slots__ = ('user_id', 'shop_user_id', 'forum_user_id', 'nickname', 'name', 'surname', 'email',
                 'email_is_verified', 'phone', 'phone_is_verified', 'password', 'vk_id', 'twitter_id', 'facebook_id',
                 'google_id', 'gender', 'birth_date', 'social', 'image_large', 'image_small', 'created_at')

    def __init__(self,
                 *,
                 user_id: Optional[int] = None,
                 shop_user_id: Optional[int] = None,
                 forum_user_id: Optional[int] = None,
                 nickname: Optional[str] = None,
                 name: Optional[str] = None,
                 surname: Optional[str] = None,
                 email: Optional[str] = None,
                 email_is_verified: Optional[bool] = None,
                 phone: Optional[str] = None,
                 phone_is_verified: Optional[bool] = None,
                 password: Optional[str] = None,
                 vk_id: Optional[str] = None,
                 twitter_id: Optional[str] = None,
                 facebook_id: Optional[str] = None,
                 google_id: Optional[str] = None,
                 gender: Optional[str] = None,
                 birth_date: Optional[str] = None,
                 social: Optional[Dict[str, str]] = None,
                 image_large: Optional[str] = None,
                 image_small: Optional[str] = None,
                 created_at: Optional[str] = None):
        """
        :param user_id: Основной идентификатор пользователя
        :param shop_user_id: Идентификатор пользователя в сервисе shop
        :param forum_user_id: Идентификатор пользователя в сервисе forum
        :param nickname: Никнейм пользователя
        :param name: Имя пользователя
        :param surname: Фамилия пользователя
        :param email: Адрес электронной почты пользователя
        :param email_is_verified: Флаг, означающий, что пользователь подтвердил адрес электронной почты (true)
        :param phone: Номер телефона пользователя
        :param phone_is_verified: Флаг, означающий, что пользователь подтвердил номер телефона (true)
        :param password: Хэш пароля пользователя (SHA-256)
        :param vk_id: Идентификатор аккаунта пользователя в ВКонтакте
        :param twitter_id: Идентификатор аккаунта пользователя в Twitter
        :param facebook_id: Идентификатор аккаунта пользователя в Facebook
        :param google_id: Идентификатор аккаунта пользователя в Google
        :param gender: Пол пользователя
        :param birth_date: Дата рождения пользователя
        :param social: Словарь, содержащий контактные данные пользователя в социальных сетях и мессенджерах
        :param image_large: Ссылка на большой аватар пользователя
        :param image_small: Ссылка на уменьшенный аватар пользователя
        :param created_at: Дата и время создания аккаунта (UTC+0)
        """
        self.user_id: Optional[int] = user_id
        self.shop_user_id: Optional[int] = shop_user_id
        self.forum_user_id: Optional[int] = forum_user_id
        self.nickname: Optional[str] = nickname
        self.name: Optional[str] = name
        self.surname: Optional[str] = surname
        self.email: Optional[str] = email
        self.email_is_verified: Optional[bool] = email_is_verified
        self.phone: Optional[str] = phone
        self.phone_is_verified: Optional[bool] = phone_is_verified
        self.password: Optional[str] = password
        self.vk_id: Optional[str] = vk_id or None
        self.twitter_id: Optional[str] = twitter_id or None
        self.facebook_id: Optional[str] = facebook_id or None
        self.google_id: Optional[str] = google_id or None
        self.gender: Optional[GenderTypes] = None
        if gender is not None:
            self.gender = GenderTypes(gender)
        self.birth_date: Optional[date] = None
        if birth_date is not None:
            self.birth_date = datetime.fromisoformat(birth_date).date()
        self.social: Optional[Dict[str, str]] = social
        self.image_large: Optional[str] = image_large
        self.image_small: Optional[str] = image_small
        self.created_at: Optional[datetime] = None
        if created_at is not None:
            self.created_at = datetime.fromisoformat(created_at)


__all__ = ['Group', 'Permission', 'TokenPair', 'Token', 'User', 'GenderTypes']
