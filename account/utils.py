def clear_dict(data: dict) -> None:
    empty_keys = [k for k, v in data.items() if v is None]
    for key in empty_keys:
        del data[key]


def replace_empty_with_none(data: dict, *keys) -> dict:
    for key in keys:
        data[key] = data[key] or None
    return data
